FROM node:18
WORKDIR /usr/src/app
COPY nodejs-chat-app/package*.json ./
RUN npm install
COPY nodejs-chat-app/. .
EXPOSE 3000
CMD [ "npm", "start" ]
