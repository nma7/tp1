# Configure Terraform to store state in a GCS bucket
terraform {  
   backend "gcs" {  
		bucket = "telecom-devops-08062023-terraform"  
		prefix = "nma_state"  
	} 
	
	required_providers {  
		google = {  source = "hashicorp/google"  
		version = "4.45.0"  }  
	} 
} 
	
# Configure the Google Cloud provider provider "google" 
provider "google" {
 project = "telecom-devops-08062023"
}

resource "google_compute_instance" "default" {
  project = "telecom-devops-08062023"
  name         = "nma-vm"
  machine_type = "e2-micro"
  zone         = "europe-west3-a"
  tags         = ["tp"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  metadata = {
        ssh-keys = "nma:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCJmCNBdvyW1efBJXZQRRi0Oz74GiClqVoKQHeEF1G+nBTF7dLeeBGn1xiqjO+XCkTXb+5XfsM8XiGyx21USIOhgLHGm3Rk4bIWvLi4gtydwJYuh4nWOVjq3+tWUw+cq7nUPIes/OFHAKrU0PAGBsePF8vGJ6gS1wWVDGV6S/oUCgXEwt2lUVm3ophOl5iIZihTUHyvEsXNbXRKnDcqXQRvIGJISKkEv+OT87iwxIh9D8/s9KW3cam5S2MHxM9Qy4RDX1nHL1RhKYLNulhaAMe6Uhb1SigXESTQdFFDmkwXTWaloLQ3Qols5MSeRO38J9izjzH07f4Zr2rQZv+8EsycvV1VRNzvKv1kSJsuBnBUtgsJBHLsnOc9lN09fzq9KkNESt76WUbUdPkEfR4ZwbDQLwnAY9M+6C/KRECOyBfgN2qTY66RpbJaPGILrICFeeES4WudgAybojqJUp6MYsvhwQmhvqMPqmT/mYKoQN+AYdtTZBD5Fm0sXEsDYmWbDk= root@d2-2-gra11-dev-h-nma-nma9"
  }


  network_interface {
        network = "default"
    access_config {
                nat_ip = google_compute_address.nma-address.address
        }
  }
}


resource "google_compute_address" "nma-address" {
  name = "nma-address2"
  region    = "europe-west3"
}

resource "google_compute_firewall" "default" {
  name    = "nma-firewall"
  network = google_compute_network.default.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "22"]
  }

  source_tags = ["web"]
}

resource "google_compute_network" "default" {
  name = "nma-network"
}

output "instance_ip_addr" {
  value = google_compute_address.nma-address.address
}
